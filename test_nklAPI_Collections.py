# -*- coding: utf-8 -*-
"""
Created on Fri Aug 27 09:17:32 2021

Created : @author: Michael Nauge (Université de Poitiers)
Forked : @pbrochard

"""


from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from NklResponse import NklResponse
from nklAPI_Datas import nklAPI_Datas


def get_collections_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un collectionIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.ffabdg37"
    nklC = nklAPI_Collections()
    r = nklC.get_collections(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un collectionIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.ffabdg38"
    r = nklC.get_collections(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe avec précision qdc >>")
    dataIdentifier = "10.34847/nkl.ffabdg38"
    r = nklC.get_collections(nklT_prodEmpty, dataIdentifier, metadataFormat="qdc")
    print(r)


def get_search_collections_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )

    print("Test d'un collectionIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.ffabdg38"
    nklC = nklAPI_Collections()
    r = nklC.search_collections(
        nklT_prodEmpty, dataIdentifier, "data", "date,asc", "1", "100"
    )
    print(r)


def post_collections_metadatas():
    # pour pouvoir modifier une data on va d'abord en creer une

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="aae99aba-476e-4ff2-2886-0aaf1bfa6fd2"
    )

    # identifier à obtenir dans la réponse de création
    rd = post_collections_test()

    if rd.isSuccess:
        identifier = rd.dictVals["payload"]["id"]
        print("le doi de la data a modifier est", identifier)

        # mise à jour de quelques metas
        metaCreator = {
            "value": "Auteur ajouté(updated by PUT alapycon)",
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/creator",
        }
        nklC = nklAPI_Collections()
        r = nklC.post_collections_metadatas(
            nklT_testUserunakala2, identifier, metaCreator
        )


def put_collections_test():
    # pour pouvoir modifier une data on va d'abord en creer une

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="aae99aba-476e-4ff2-2886-0aaf1bfa6fd2"
    )

    # identifier à obtenir dans la réponse de création
    rd = post_collections_test()

    if rd.isSuccess:
        identifier = rd.dictVals["payload"]["id"]
        print("le doi de la data a modifier est", identifier)

        # mise à jour de quelques metas
        dicoMetas = {}
        dicoMetas["metas"] = []
        # création de la méta nakala_title
        # c'est la seule méta obligatoire
        metaTitle = {
            "value": "DBG - Collection by nakalapycon (updated by PUT nakalapycon)",
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://nakala.fr/terms#title",
        }
        dicoMetas["metas"].append(metaTitle)
        nklC = nklAPI_Collections()
        r = nklC.put_collections(nklT_testUserunakala2, identifier, dicoMetas)
        print(r)


def delete_collections_test():
    # pour pouvoir tester la suppression d'une collection il faut en creer une avant.

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # identifier à obtenir dans la réponse de création
    rd = post_collections_test()

    if rd.isSuccess:
        identifier = rd.dictVals["payload"]["id"]
        print("le doi de la data a supprimmer est", identifier)

        nklC = nklAPI_Collections()
        r = nklC.delete_collections(nklT_testUserunakala2, identifier)
        print(r)


def post_collections_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="aae99aba-476e-4ff2-2886-0aaf1bfa6fd2"
    )

    # creation d'un dictionnaire collection
    dicoMetas = {}

    # choisir la valeur du status de la collection
    # soit public soit private
    dicoMetas["status"] = "public"

    dicoMetas["metas"] = []
    # création de la méta nakala_title
    # c'est la seule méta obligatoire
    metaTitle = {
        "value": "DBG - Collection by nakalapycon",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#title",
    }
    dicoMetas["metas"].append(metaTitle)

    print(dicoMetas)
    nklC = nklAPI_Collections()
    rd = nklC.post_collections(nklT_testUserunakala2, dicoMetas)
    print(rd)

    if rd.isSuccess:
        print("le doi de la collection est", rd.dictVals["payload"]["id"])

    return rd


def get_collections_datas_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un collectionIdentifier qui n'existe pas >> ")
    dataIdentifier = "11280/f02feb10"

    nklC = nklAPI_Collections()
    r = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe  >>")
    dataIdentifier = "11280/f02feb10"
    r = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier)
    print(r)

    nbDataInThisPage = len(r.dictVals["data"])
    print("nbDataInThisPage", nbDataInThisPage)

    # afficher le nombre de page de resultat disponible:
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
        print("nb page dispo: ", lastPage)

        # on appel la dernière page
        r_last = nklC.get_collections_datas(
            nklT_prodEmpty, dataIdentifier, page=lastPage
        )
        print(r_last)

        nbDataInThisPage = len(r_last.dictVals["data"])
        print("nbDataInThisPage", nbDataInThisPage)


def post_collections_datas_test():
    # on va dabord creer une data puis on va crééer une collection et enfin on va ajouter la data dans la collection

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )
    nklC = nklAPI_Collections()
    # creation d'un dictionnaire data
    dicoMetas = {}
    # dicoMetas["collectionsIds"] = ["10.34847/nkl.b9eblul0"]
    dicoMetas["collectionsIds"] = []

    dicoMetas["status"] = "pending"
    # dicoMetas["status"] = "published"

    dicoMetas["metas"] = []

    metaType = {
        "value": "http://purl.org/coar/resource_type/c_c513",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
        "propertyUri": "http://nakala.fr/terms#type",
    }
    dicoMetas["metas"].append(metaType)

    metaTitle = {
        "value": "DBG - data image (nakalapycon)",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#title",
    }
    dicoMetas["metas"].append(metaTitle)

    metaAuthor = {
        "value": {"givenname": "Mic", "surname": "Mac", "orcid": "0000-0000-4242-4242"},
        "propertyUri": "http://nakala.fr/terms#creator",
    }
    dicoMetas["metas"].append(metaAuthor)

    metaCreated = {
        "value": "1961-01-01",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#created",
    }
    dicoMetas["metas"].append(metaCreated)

    metaLicense = {
        "value": "CC-BY-4.0",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#license",
    }
    dicoMetas["metas"].append(metaLicense)

    metaSubjectFr = {
        "value": "sujetEnFre",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/subject",
    }
    dicoMetas["metas"].append(metaSubjectFr)

    metaSubjectEn = {
        "value": "sujetEnEng",
        "lang": "en",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/subject",
    }
    dicoMetas["metas"].append(metaSubjectEn)

    # ajout d'un group utilisateur avec les droits admin
    """
    dicoMetas["rights"]=[]
    dicoMetas["rights"].append({'id':"cb5f5980-056e-11ec-9b31-52540084ccd3",'role':"ROLE_ADMIN"})
    """

    # ajout d'un role admin à unakala3 8badb61c-ad90-11eb-8529-0242ac130003
    dicoMetas["rights"] = []
    dicoMetas["rights"].append(
        {"id": "8badb61c-ad90-11eb-8529-0242ac130003", "role": "ROLE_ADMIN"}
    )

    # envoi d'un fichier pour obtenir son sha1
    pathFile = "./test.jpg"
    nklD = nklAPI_Datas()
    r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)

    print(r)

    if r.isSuccess:
        print("le sha1 est", r.dictVals["sha1"])

        dicoMetas["files"] = [
            {
                "sha1": r.dictVals["sha1"],
                "description": "ma belle description de fichier",
            }
        ]

        # puisque l'envoi du fichier c'est bien passé,
        # on envoi la data
        rd = nklD.post_datas(nklT_testUserunakala2, dicoMetas)
        print(rd)

        if rd.isSuccess:
            identifierData = rd.dictVals["payload"]["id"]
            print("le doi de la data est", identifierData)

            # on va creer une collection
            # identifier à obtenir dans la réponse de création
            rc = post_collections_test()

            if rc.isSuccess:
                identifierCollection = rc.dictVals["payload"]["id"]

                # on va ajouter la nouvelle data dans cette nouvelle collection
                listData = [identifierData]
                rp = nklC.post_collections_datas(
                    nklT_testUserunakala2, identifierCollection, listData
                )
                print(rp)


# get_search_collections_test()
get_collections_test()
# post_collections_test()
# put_collections_test()
#post_collections_metadatas()
# delete_collections_test()
#get_collections_datas_test()
# post_collections_datas_test()
