# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 15:50:10 2021

Created : @author: Michael Nauge (Université de Poitiers)

"""

import requests
import json

from NklResponse import NklResponse


class nklAPI_Users:
    def get_users_me(self, nklTarget):
        """
        Récupération des informations sur l'utilisateur courant
        Retourne l'ensemble des informations disponibles sur l'utilisateur courant

        Parameters
        nklTarget : OBJ
            une instance d'un objet NklTarget permettant de choisir nakala_prod ou nakala_test




        Returns
        -------
        NklResponse : OBJ
            une instance d'un objet NklResponse
            - en cas de problème reseau ou de message d'erreur du serveur nakala
            l'objet NklResponse.isSuccess=False et le détails de l'erreur en version textuel
            sera dans NklResponse.message

            - en cas de réussite de la requete
            l'objet NklResponse.isSuccess=True et
            les métadonnées reçu seront dans
            NklResponse.dictVals : un dictionnaire contenant les metadatas obtenus depuis la reponse json du server


        """

        url = nklTarget.API_URL + "/users/me"

        APIheaders = {}

        # on gère le cas où la data est public
        # et qu'il n'y a donc pas besoin de API_KEY
        if nklTarget.apiKey_isEmpty() == False:
            APIheaders = {"X-API-KEY": nklTarget.API_KEY_NKL}

        # création d'une instance d'un objet NklResponse à retourner
        nklR = NklResponse()

        try:
            response = requests.get(url, headers=APIheaders)
            # on récupère le code
            nklR.code = response.status_code

            # 200 le serveur a bien répondu
            if response.status_code == 200:
                nklR.isSuccess = True
                nklR.message = "Informations sur l'utilisateur courant"
                # on converti l'objet json retournée en dictionnaire python
                nklR.dictVals = json.loads(response.text)

                # on retourne l'objet NklResponse maintenant entièrement rempli
                return nklR

            else:
                dicError = json.loads(response.text)
                nklR.message = dicError["message"]

        except requests.exceptions.RequestException as e:
            nklR.code = -1
            nklR.message = e

        # on retourne l'objet NklResponse avec erreur (de nakala ou de connexion reseau)
        return nklR

    def post_users_datas(self, nklTarget, scope="deposited", page=1, limit=10):
        """
        Récupération de la liste paginée des données déposées par un utilisateur

        Parameters
        nklTarget : OBJ
            une instance d'un objet NklTarget permettant de choisir nakala_prod ou nakala_test

        identifier : STR
            un COLLECTION identifier nakala.

        scope : STR
            périmètre
                deposited : les données déposées par l'utilisateur (ROLE_DEPOSITOR)
                owned : les données dont l'utilisateur est propriétaire (ROLE_OWNER)
                shared : les données partagées avec l'utilisateur (ROLE_ADMIN, ROLE_EDITOR ou ROLE_READER, mais pas ROLE_OWNER)
                editable : les données modifiables par l'utilisateur (ROLE_OWNER, ROLE_ADMIN ou ROLE_EDITOR)
                readable : les données lisibles par l'utilisateur (ROLE_OWNER, ROLE_ADMIN, ROLE_EDITOR ou ROLE_READER)
                all : toutes les données auxquelles l'utilisateur à accès (ROLE_OWNER, ROLE_ADMIN, ROLE_EDITOR ou ROLE_READER) ainsi que toutes les données publiées de NAKALA

        page : INT
            La page souhaitée

        limit : INT
            Le nombre de résultat par page


        Returns
        -------
        NklResponse : OBJ
            une instance d'un objet NklResponse
            - en cas de problème reseau ou de message d'erreur du serveur nakala
            l'objet NklResponse.isSuccess=False et le détails de l'erreur en version textuel
            sera dans NklResponse.message

            - en cas de réussite de la requete
            l'objet NklResponse.isSuccess=True et
            les métadonnées reçu seront dans
            NklResponse.dictVals : un dictionnaire contenant les metacollections obtenus depuis la reponse json du server


        """
        # ​/users/datas/{scope}
        url = nklTarget.API_URL + "/users/datas/" + scope
        APIheaders = {}

        # on gère le cas où il n'y a pas de API_KEY ce qui posera problème au server
        # mais il nous le fera savoir !
        if nklTarget.apiKey_isEmpty() == False:
            APIheaders = {
                "X-API-KEY": nklTarget.API_KEY_NKL,
                "accept": "application/json",
                "Content-Type": "application/json",
            }

        # création d'une instance d'un objet NklResponse à retourner
        nklR = NklResponse()

        datas = {}
        datas["page"] = page
        datas["limit"] = limit

        try:
            response = requests.post(url, data=json.dumps(datas), headers=APIheaders)
            # on récupère le code
            nklR.code = response.status_code

            # 200 le serveur a bien répondu
            if response.status_code == 200:
                nklR.isSuccess = True
                nklR.message = "Informations sur la collection"
                # on converti l'objet json retournée en dictionnaire python
                nklR.dictVals = json.loads(response.text)

                # on retourne l'objet NklResponse maintenant entièrement rempli
                return nklR

            else:
                dicError = json.loads(response.text)
                nklR.message = dicError["message"]

        except requests.exceptions.RequestException as e:
            nklR.code = -1
            nklR.message = e

        # on retourne l'objet NklResponse avec erreur (de nakala ou de connexion reseau)
        return nklR

    def post_users_collections(self, nklTarget, scope="deposited", page=1, limit=10):
        """
        Récupération de la liste paginée des collections crées par un utilisateur

        Parameters
        nklTarget : OBJ
            une instance d'un objet NklTarget permettant de choisir nakala_prod ou nakala_test

        scope : STR
            périmètre
                deposited : les collections déposées par l'utilisateur (ROLE_DEPOSITOR)
                owned : les collections dont l'utilisateur est propriétaire (ROLE_OWNER)
                shared : les collections partagées avec l'utilisateur (ROLE_ADMIN, ROLE_EDITOR ou ROLE_READER, mais pas ROLE_OWNER)
                editable : les collections modifiables par l'utilisateur (ROLE_OWNER, ROLE_ADMIN ou ROLE_EDITOR)
                readable : les collections lisibles par l'utilisateur (ROLE_OWNER, ROLE_ADMIN, ROLE_EDITOR ou ROLE_READER)
                all : toutes les collections auxquelles l'utilisateur à accès (ROLE_OWNER, ROLE_ADMIN, ROLE_EDITOR ou ROLE_READER) ainsi que toutes les collections publiques de NAKALA

        page : INT
            La page souhaitée

        limit : INT
            Le nombre de résultat par page


        Returns
        -------
        NklResponse : OBJ
            une instance d'un objet NklResponse
            - en cas de problème reseau ou de message d'erreur du serveur nakala
            l'objet NklResponse.isSuccess=False et le détails de l'erreur en version textuel
            sera dans NklResponse.message

            - en cas de réussite de la requete
            l'objet NklResponse.isSuccess=True et
            les métadonnées reçu seront dans
            NklResponse.dictVals : un dictionnaire contenant les metacollections obtenus depuis la reponse json du server

        """
        # ​/users/collections/{scope}
        url = nklTarget.API_URL + "/users/collections/" + scope
        APIheaders = {}

        # on gère le cas où il n'y a pas de API_KEY ce qui posera problème au server
        # mais il nous le fera savoir !
        if nklTarget.apiKey_isEmpty() == False:
            APIheaders = {
                "X-API-KEY": nklTarget.API_KEY_NKL,
                "accept": "application/json",
                "Content-Type": "application/json",
            }

        # création d'une instance d'un objet NklResponse à retourner
        nklR = NklResponse()

        datas = {}
        datas["page"] = page
        datas["limit"] = limit

        try:
            response = requests.post(url, data=json.dumps(datas), headers=APIheaders)
            # on récupère le code
            nklR.code = response.status_code

            # 200 le serveur a bien répondu
            if response.status_code == 200:
                nklR.isSuccess = True
                nklR.message = "Informations sur la collection"
                # on converti l'objet json retournée en dictionnaire python
                nklR.dictVals = json.loads(response.text)

                # on retourne l'objet NklResponse maintenant entièrement rempli
                return nklR

            else:
                dicError = json.loads(response.text)
                nklR.message = dicError["message"]

        except requests.exceptions.RequestException as e:
            nklR.code = -1
            nklR.message = e

        # on retourne l'objet NklResponse avec erreur (de nakala ou de connexion reseau)
        return nklR
