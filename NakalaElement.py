# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:28:54 2022

Created : @author: pbrochard

"""


import requests
import logging
import re


class NakalaElement(object):
    """
    Turns a dictionary into a class
    """

    def __init__(self, lang):
        """init"""
        self.lang = lang
        self.listeMeta = []
        self.listeNakalaCreators = []
        self.listeCreators = []
        self.listeCreatorsURI = []
        self.listeSubject = []
        self.listeSubjectURI = []
        self.listeSpatial = []
        self.listeSpatialURI = []
        self.listeSource = []
        self.listeSourceURI = []
        self.listeisReferencedBy = []
        self.listeisReferencedByURI = []
        self.listeisRequiredByURI = []
        self.listePublisher = []
        self.listePublisherURI = []
        self.listeProvenance = []
        self.listeProvenanceURI = []
        self.listeModified = []
        self.listRelation = []
        self.listRelationURI = []

    def fromNakalaDict(self, dictionary, lang="fr", path="."):
        """Constructor"""
        setattr(self, "identifier", dictionary["identifier"])
        setattr(self, "path", path)
        listeNakalaCreators = []
        listeCreators = []
        listeCreatorsURI = []
        listeLanguage = []
        listeSubject = []
        listeSubjectURI = []
        listeSpatial = []
        listeSpatialURI = []
        listeSource = []
        listeSourceURI = []
        listeisReferencedBy = []
        listeisReferencedByURI = []
        listeisRequiredByURI = []
        listePublisher = []
        listePublisherURI = []
        listeProvenance = []
        listeProvenanceURI = []
        listeModified = []
        listeRelation = []
        listeRelationURI = []
        for element in dictionary["metas"]:
            if (
                element["propertyUri"] == "http://nakala.fr/terms#title"
                and element["lang"] == lang
            ):
                setattr(self, "title", element["value"])
            if element["propertyUri"] == "http://nakala.fr/terms#created":
                setattr(self, "created", element["value"])

            if element["propertyUri"] == "http://purl.org/dc/terms/language":
                listeLanguage.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/description"
                and element["lang"] == lang
            ):
                setattr(self, "description", element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/creator"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeCreators.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/creator"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeCreatorsURI.append(element["value"])
            if (
                element["propertyUri"] == "http://nakala.fr/terms#creator"
                and element["value"] is not None
            ):
                listeNakalaCreators.append(
                    (
                        element["value"]["givenname"],
                        element["value"]["surname"],
                        element["value"]["orcid"],
                    )
                )
            if element["propertyUri"] == "http://purl.org/dc/terms/hasversion":
                setattr(self, "hasversion", element["value"])
            if element["propertyUri"] == "http://purl.org/dc/terms/date":
                setattr(self, "date", element["value"])
            if (
                element["propertyUri"]
                == "http://purl.org/dc/terms/bibliographicCitation"
            ):
                setattr(self, "bibliographicCitation", element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/description"
                and element["lang"] == lang
            ):
                setattr(self, "description", element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/subject"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
                and element["lang"] == lang
            ):
                listeSubject.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/subject"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeSubjectURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/spatial"
                and element["lang"] == lang
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeSpatial.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/spatial"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeSpatialURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/source"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeSource.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/source"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeSourceURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/isReferencedBy"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeisReferencedBy.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/isReferencedBy"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeisReferencedByURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/isRequiredBy"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeisRequiredByURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/publisher"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listePublisher.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/publisher"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listePublisherURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/modified"
                and element["typeUri"] == "http://purl.org/dc/terms/W3CDTF"
            ):
                listeModified.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/provenance"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeProvenance.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/provenance"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeProvenanceURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/relation"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeRelation.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/relation"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeRelationURI.append(element["value"])

        setattr(self, "creator", listeCreators)
        setattr(self, "language", listeLanguage)
        setattr(self, "creatorURI", listeCreatorsURI)
        setattr(self, "nakalacreator", listeNakalaCreators)
        setattr(self, "subject", listeSubject)
        setattr(self, "subjectURI", listeSubjectURI)
        setattr(self, "spatial", listeSpatial)
        setattr(self, "spatialURI", listeSpatialURI)
        setattr(self, "source", listeSource)
        setattr(self, "sourceURI", listeSourceURI)
        setattr(self, "isReferencedBy", listeisReferencedBy)
        setattr(self, "isReferencedByURI", listeisReferencedByURI)
        setattr(self, "isRequiredByURI", listeisRequiredByURI)
        setattr(self, "publisher", listePublisher)
        setattr(self, "publisherURI", listePublisherURI)
        setattr(self, "provenance", listeProvenance)
        setattr(self, "provenanceURI", listeProvenanceURI)
        setattr(self, "modified", listeModified)
        setattr(self, "relation", listeRelation)
        setattr(self, "relationURI", listeRelationURI)
        # logging.info("Instantiation de lobject NakalaElement")

    def getDictMetaURI(self):
        """
        renvoie le dict["metas"]
        """
        vocab = "http://purl.org/dc/terms/"
        listeVocab = [
            "subjectURI",
            "descriptionURI",
            "publisherURI",
            "creatorURI",
            "contributorURI",
            "dateURI",
            "typeURI",
            "formatURI",
            "identifierURI",
            "sourceURI",
            "languageURI",
            "relationURI",
            "coverageURI",
            "rightsURI",
            "audienceURI",
            "alternativeURI",
            "tableOfContentsURI",
            "abstractURI",
            "createdURI",
            "validURI",
            "availableURI",
            "issuedURI",
            "modifiedURI",
            "extentURI",
            "mediumURI",
            "isversionofURI",
            "hasversionURI",
            "isreplacedbyURI",
            "replacesURI",
            "isRequiredByURI",
            "requiresURI",
            "isPartOfURI",
            "hasPartURI",
            "isReferencedByURI",
            "referencesURI",
            "isFormatOfURI",
            "hasFormatURI",
            "conformsToURI",
            "spatialURI",
            "temporalURI",
            "mediatorURI",
            "dateAcceptedURI",
            "dateCopyrightedURI",
            "dateSubmittedURI",
            "educationLevelURI",
            "accessRightsURI",
            "bibliographiccitationURI",
            "licenseURI",
            "rightsHolderURI",
            "provenanceURI",
            "instructionalMethodURI",
            "accrualMethodURI",
            "accrualPeriodicityURI",
            "accrualPolicyURI",
        ]
        for param, element in self.__dict__.items():
            if type(getattr(self, param)) == str and param in listeVocab:
                self.listeMeta.append(
                    {
                        "value": element,
                        "lang": self.lang,
                        "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
                        "propertyUri": vocab + param[:-3],
                    }
                )
            if type(getattr(self, param)) == list and param in listeVocab:
                for elem in element:
                    self.listeMeta.append(
                        {
                            "value": elem,
                            "lang": self.lang,
                            "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
                            "propertyUri": vocab + param[:-3],
                        }
                    )

    def getDictMetaString(self):
        """
        renvoie le dict["metas"]
        """
        listeNakala = ["title", "license", "created"]
        listeVocab = [
            "subject",
            "description",
            "publisher",
            "creator",
            "contributor",
            "date",
            "type",
            "format",
            "identifier",
            "source",
            "language",
            "relation",
            "coverage",
            "rights",
            "audience",
            "alternative",
            "tableOfContents",
            "abstract",
            "valid",
            "available",
            "issued",
            "extent",
            "medium",
            "isVersionOf",
            "hasVersion",
            "isReplacedBy",
            "replaces",
            "isRequiredBy",
            "requires",
            "isPartOf",
            "hasPart",
            "isReferencedBy",
            "references",
            "isFormatOf",
            "hasFormat",
            "conformsTo",
            "spatial",
            "temporal",
            "mediator",
            "dateAccepted",
            "dateCopyrighted",
            "dateSubmitted",
            "educationLevel",
            "accessRights",
            "bibliographicCitation",
            "rightsHolder",
            "provenance",
            "instructionalMethod",
            "accrualMethod",
            "accrualPeriodicity",
            "accrualPolicy",
        ]
        for param, element in self.__dict__.items():
            if param in listeNakala:
                vocab = "http://nakala.fr/terms#"
            else:
                vocab = "http://purl.org/dc/terms/"

            if type(getattr(self, param)) == str and param in [*listeVocab, "title"]:
                self.listeMeta.append(
                    {
                        "value": element,
                        "lang": self.lang,
                        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                        "propertyUri": vocab + param,
                    }
                )
            if type(getattr(self, param)) == list and param in [*listeVocab, "title"]:
                for elem in element:
                    self.listeMeta.append(
                        {
                            "value": elem,
                            "lang": self.lang,
                            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                            "propertyUri": vocab + param,
                        }
                    )
            if type(getattr(self, param)) == str and param in ["created", "license" ]:
                self.listeMeta.append(
                    {
                        "value": element,
                        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                        "propertyUri": vocab + param,
                    }
                )

    def setMeta(self, param, value):
        """
        ajouter la metadonnee
        """
        setattr(self, param, value)

    def getMeta(self, param):
        """
        ajouter la metadonnee
        """
        return getattr(self, param)

    def setMetaAuthor(self, liste):
        """modification givenname, surname, orcid"""
        for element in liste:
            self.listeNakalaCreators.append(element)

    def getMetaAuthor(self):
        """modification givenname, surname, orcid"""
        for element in self.listeNakalaCreators:
            self.listeMeta.append(
                {
                    "value": {
                        "givenname": element[0],
                        "surname": element[1],
                        "orcid": element[2],
                    },
                    "propertyUri": "http://nakala.fr/terms#creator",
                }
            )

    def setDictMetaAuthorDirect(self, givenname):
        """
        mettre l'auteur
        """
        self.listeMeta.append(
            {
                "value": {"givenname": givenname, "surname": "", "orcid": ""},
                "propertyUri": "http://nakala.fr/terms#creator",
            }
        )

    def getMetaNakala(self):
        """
        renvoie les metadonnees au format Nakala
        """
        self.getDictMetaURI()
        self.getDictMetaString()
        self.getMetaAuthor()
        return self.listeMeta

    def setType(self, materiel):
        """
        le type de documents
        """
        if materiel == "txt":
            c = "c_18cf"
        else:
            c = "c_c513"
        self.listeMeta.append(
            {
                "value": "http://purl.org/coar/resource_type/" + c,
                "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
                "propertyUri": "http://nakala.fr/terms#type",
            }
        )

    def debug_data(self):
        print(self.__dict__)
