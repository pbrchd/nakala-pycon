# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:28:54 2022

Created : @author: pbrochard
"""

import requests
from NakalaElement import NakalaElement
from NklTarget import NklTarget
from nklAPI_Datas import nklAPI_Datas
import logging
import json
import time


class SemanticElement(object):
    """
    mets en cache les elements en semantic
    """

    # ----------------------------------------------------------------------
    def __init__(self, lang):
        """Constructor"""
        dictRequetes = {}
        setattr(self, "dictRequetes", dictRequetes)
        setattr(self, "lang", lang)
        logging.info("Instantiation du Module SemanticElement avec la lang " + lang)

    def interrogation(self, requete):
        """filtrage des requetes"""
        if "http://www.wikidata.org/entity/" in requete:
            if requete not in self.dictRequetes:
                time.sleep(15)
                self.dictRequetes[requete] = self.getWikidata(requete)
            else:
                logging.info("SemanticElement : on fait jouer le cache")
            return self.dictRequetes[requete]
        if "http://www.nakala.fr" in requete:
            if requete not in self.dictRequetes:
                time.sleep(15)
                self.dictRequetes[requete] = self.getNakala(requete)
            else:
                logging.info("SemanticElement : on fait jouer le cache")
            return self.dictRequetes[requete]
        if "http://www.idref.fr" in requete:
            if requete not in self.dictRequetes:
                time.sleep(15)
                self.dictRequetes[requete] = self.getIdref(requete)
            else:
                logging.info("SemanticElement : on fait jouer le cache")
            return self.dictRequetes[requete]
        else:
            logging.error("Erreur dans la requete", requete)

    def interrogationId(self, requete):
        """filtrage des requetes"""
        if "http://www.wikidata.org/entity/" in requete:
            if requete not in self.dictRequetes:
                time.sleep(15)
                self.dictRequetes[requete] = self.getWikidataId(requete)
            else:
                logging.info("SemanticElement : on fait jouer le cache")
            return self.dictRequetes[requete]

    def getWikidata(self, iD):
        """retourne le simple nom"""
        latitude = ""
        longitude = ""
        logging.info("Interrogation Wikidata : " + str(iD) + "avec la langue" + self.lang)
        query = (
            """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT
        ?item
        ?itemLabel
        ?itemDescription
        ?coordonnees_geographiques
        WHERE
        {
        VALUES (?item)
        {
            (<"""
            + iD
            + '''>)
        }
        SERVICE wikibase:label
        { bd:serviceParam wikibase:language "'''
            + self.lang
            + """" }
        OPTIONAL { ?item wdt:P625 ?coordonnees_geographiques.}
        }
        """
        )
        url = "https://query.wikidata.org/bigdata/namespace/wdq/sparql"
        try:
            wikidata = requests.get(url, params={"query": query, "format": "json"})
        except requests.ConnectionError as e:
            logging.error(str(e))
        except requests.exceptions.Timeout as e:
            logging.error(str(e))
        except requests.exceptions.TooManyRedirects as e:
            logging.error(str(e))
        else:
            try:
                data = wikidata.json()
            except json.decoder.JSONDecodeError as e:
                logging.error(str(e))
                logging.error(str(data))
            else:
                label = data["results"]["bindings"][0]["itemLabel"]["value"]
                description = data["results"]["bindings"][0]["itemDescription"]["value"]
                try:
                    temp = data["results"]["bindings"][0]["coordonnees_geographiques"][
                        "value"
                    ]
                except:
                    logging.error("Pas de coordonnees geographiques")
                    longitude = ""
                    latitude = ""
                else:
                    longitude = temp.split(" ")[0][6:]
                    latitude = temp.split(" ")[1][:-1]
        return [label, iD, longitude, latitude, description]

    def getWikidataId(self, iD):
        """retourne les identifiants"""
        latitude = ""
        longitude = ""
        logging.info("Interrogation Wikidata Identifiants: " + str(iD))
        query = (
            """PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        SELECT
        ?item
        ?itemLabel
        ?itemIdref
        ?itemBnf
        ?itemGeonames
        WHERE
        {
        VALUES (?item)
        {
            (<"""
            + iD
            + '''>)
        }
        SERVICE wikibase:label
        { bd:serviceParam wikibase:language "'''
            + self.lang
            + """" }
        OPTIONAL { ?item wdt:P269 ?itemIdref.}
        OPTIONAL { ?item wdt:P268 ?itemBnf.}
        OPTIONAL { ?item wdt:P1566 ?itemGeonames.}
        }
        """
        )
        url = "https://query.wikidata.org/bigdata/namespace/wdq/sparql"
        try:
            wikidata = requests.get(url, params={"query": query, "format": "json"})
        except requests.ConnectionError as e:
            logging.error(str(e))
        except requests.exceptions.Timeout as e:
            logging.error(str(e))
        except requests.exceptions.TooManyRedirects as e:
            logging.error(str(e))
        else:
            try:
                data = wikidata.json()
            except json.decoder.JSONDecodeError as e:
                logging.error(str(e))
                logging.error(str(data))
            else:
                label = data["results"]["bindings"][0]["itemLabel"]["value"]
                try:
                    idref = (
                        "http://www.idref.fr/"
                        + data["results"]["bindings"][0]["itemIdref"]["value"]
                        + "/id"
                    )
                except:
                    logging.error("Pas d'idRef pour %s" % (iD))
                    idref = None
                try:
                    bnf = (
                        "https://catalogue.bnf.fr/ark:/12148/cb"
                        + data["results"]["bindings"][0]["itemBnf"]["value"]
                    )
                except:
                    logging.error("Pas de Bnf pour %s" % (iD))
                    bnf = None
                try:
                    geonames = (
                        "https://www.geonames.org/"
                        + data["results"]["bindings"][0]["itemGeonames"]["value"]
                    )
                except:
                    logging.error("Pas de geonames pour %s" % (iD))
                    geonames = None
        idref = ""
        bnf = ""
        iD = ""
        geonames = ""
        return [iD, idref, bnf, geonames]

    def getIdref(self, iD):
        """retourne le nom Idref"""
        logging.info("Interrogation IdRef : " + str(iD))
        query = (
            """PREFIX foaf: <http://xmlns.com/foaf/0.1/>
            SELECT
            ?nom
            WHERE { <"""
            + iD
            + """> foaf:name ?nom.
            }
            """
        )
        url = "https://data.idref.fr/sparql"
        try:
            idrefdata = requests.get(url, params={"query": query, "format": "json"})
        except requests.ConnectionError as e:
            logging.error(str(e))
        except requests.exceptions.Timeout as e:
            logging.error(str(e))
        except requests.exceptions.TooManyRedirects as e:
            logging.error(str(e))
        else:
            try:
                data = idrefdata.json()
            except json.decoder.JSONDecodeError as e:
                logging.error(str(e))
                logging.error(str(data))
            else:
                label = data["results"]["bindings"][0]["nom"]["value"]
        return label

    def getSudoc(self, iD):
        """retourne la citation bibliographique"""
        logging.info("Interrogation Sudoc : " + str(iD))
        query = (
            """PREFIX dcterms: <http://purl.org/dc/terms/>
            SELECT
            distinct 
            ?citation 
            WHERE { <"""
            + iD
            + """> dcterms:bibliographicCitation ?citation.
            }
            """
        )
        url = "https://data.idref.fr/sparql"
        try:
            sudocdata = requests.get(url, params={"query": query, "format": "json"})
        except requests.ConnectionError as e:
            logging.error(str(e))
        except requests.exceptions.Timeout as e:
            logging.error(str(e))
        except requests.exceptions.TooManyRedirects as e:
            logging.error(str(e))
        else:
            try:
                data = sudocdata.json()
            except json.decoder.JSONDecodeError as e:
                logging.error(str(e))
                logging.error(str(data))
            else:
                label = data["results"]["bindings"][0]["nom"]["value"]
        return label

    def debug(self):
        print(self.dictRequetes)
