# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

Created : @author: Michael Nauge (Université de Poitiers)
Forked : @pbrochard

"""


from NklTarget import NklTarget
from nklAPI_Datas import nklAPI_Datas
import NklTarget as nklT


def get_datas_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.8a0czf6y"
    nklD = nklAPI_Datas()
    r = nklD.get_datas(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.be633595"
    r = nklD.get_datas(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe avec précision qdc >>")
    dataIdentifier = "10.34847/nkl.be633595"
    r = nklD.get_datas(nklT_prodEmpty, dataIdentifier, metadataFormat="qdc")
    print(r)


def put_datas_test():
    # pour pouvoir modifier une data on va d'abord en creer une

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )

    # identifier à obtenir dans la réponse de création
    rd = post_datas_test()

    if rd.isSuccess:
        identifier = rd.dictVals["payload"]["id"]
        print("le doi de la data a modifier est", identifier)

        # mise à jour de quelques metas

        dicoMetas = {}
        # dicoMetas["collectionsIds"] = ["10.34847/nkl.b9eblul0"]
        dicoMetas["collectionsIds"] = []

        # dicoMetas["status"] = "pending"
        dicoMetas["status"] = "published"

        dicoMetas["metas"] = []

        metaType = {
            "value": "http://purl.org/coar/resource_type/c_c513",
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
            "propertyUri": "http://nakala.fr/terms#type",
        }
        dicoMetas["metas"].append(metaType)

        metaTitle = {
            "value": "DBG - data image upadated by PUT (nakalapycon)",
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://nakala.fr/terms#title",
        }
        dicoMetas["metas"].append(metaTitle)

        metaAuthor = {
            "value": {
                "givenname": "Mic",
                "surname": "Mac",
                "orcid": "0000-0000-4242-4242",
            },
            "propertyUri": "http://nakala.fr/terms#creator",
        }
        dicoMetas["metas"].append(metaAuthor)

        metaCreated = {
            "value": "1942-01-01",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://nakala.fr/terms#created",
        }
        dicoMetas["metas"].append(metaCreated)

        metaLicense = {
            "value": "CC-BY-4.0",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://nakala.fr/terms#license",
        }
        dicoMetas["metas"].append(metaLicense)

        metaSubjectFr = {
            "value": "agréable",
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/subject",
        }
        dicoMetas["metas"].append(metaSubjectFr)

        metaSubjectEn = {
            "value": "cool",
            "lang": "fr",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/subject",
        }
        dicoMetas["metas"].append(metaSubjectEn)

        nklD = nklAPI_Datas()
        r = nklD.put_datas(nklT_testUserunakala2, identifier, dicoMetas)
        print(r)


def delete_datas_test():
    # pour pouvoir tester la suppression d'une data il faut en creer une avant.

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # identifier à obtenir dans la réponse de création

    rd = post_datas_test()

    if rd.isSuccess:
        identifier = rd.dictVals["payload"]["id"]
        print("le doi de la data a supprimmer est", identifier)

        r = nklD.delete_datas(nklT_testUserunakala2, identifier)
        print(r)


def post_datas_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )

    # creation d'un dictionnaire data
    dicoMetas = {}
    #    dicoMetas["collectionsIds"] = ["10.34847/nkl.a25ellkx"]

    # dicoMetas["status"] = "pending"
    dicoMetas["status"] = "published"

    dicoMetas["metas"] = []

    metaType = {
        "value": "http://purl.org/coar/resource_type/c_c513",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
        "propertyUri": "http://nakala.fr/terms#type",
    }
    dicoMetas["metas"].append(metaType)

    metaTitle = {
        "value": "DBG - data image (nakalapycon)",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#title",
    }
    dicoMetas["metas"].append(metaTitle)

    metaAuthor = {
        "value": {"givenname": "Mic", "surname": "Mac", "orcid": "0000-0000-4242-4242"},
        "propertyUri": "http://nakala.fr/terms#creator",
    }
    dicoMetas["metas"].append(metaAuthor)

    metaCreated = {
        "value": "1961-01-01",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#created",
    }
    dicoMetas["metas"].append(metaCreated)

    metaLicense = {
        "value": "CC-BY-4.0",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://nakala.fr/terms#license",
    }
    dicoMetas["metas"].append(metaLicense)

    metaSubjectFr = {
        "value": "sujetEnFre",
        "lang": "fr",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/subject",
    }
    dicoMetas["metas"].append(metaSubjectFr)

    metaSubjectEn = {
        "value": "sujetEnEng",
        "lang": "en",
        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
        "propertyUri": "http://purl.org/dc/terms/subject",
    }
    dicoMetas["metas"].append(metaSubjectEn)

    """
    # ajout d'un group utilisateur avec les droits admin
    dicoMetas["rights"]=[]
    dicoMetas["rights"].append({'id':"cb5f5980-056e-11ec-9b31-52540084ccd3",'role':"ROLE_ADMIN"})
    
    #ajout d'un role admin à unakala3 8badb61c-ad90-11eb-8529-0242ac130003
    dicoMetas["rights"]=[]
    dicoMetas["rights"].append({'id':"8badb61c-ad90-11eb-8529-0242ac130003",'role':"ROLE_ADMIN"})
    """
    # envoi d'un fichier pour obtenir son sha1
    pathFile = "/tmp/test.png"
    nklD = nklAPI_Datas()
    r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)

    if r.isSuccess:
        dicoMetas["files"] = [
            {
                "sha1": r.dictVals["sha1"],
                "description": "ma belle description de fichier",
            }
        ]

        # puisque l'envoi du fichier c'est bien passé,
        # on envoi la data
        rd = nklD.post_datas(nklT_testUserunakala2, dicoMetas)

        if rd.isSuccess:
            print("le doi de la data est", rd.dictVals["payload"]["id"])

    return rd


def get_datas_files_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.be633595"

    rf = nklD.get_datas_files(nklT_prodEmpty, dataIdentifier)

    print(rf)


def post_datas_files_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # creation d'une nouvelle data
    rd = post_datas_test()

    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]

        # envoi d'un nouveau fichier pour obtenir son sha1
        pathFile = "./../test/files/test_0001-0002.jpg"
        r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)

        print(r)

        if r.isSuccess:
            print("le sha1 est", r.dictVals["sha1"])

            fileInfos = {
                "sha1": r.dictVals["sha1"],
                "description": "ma belle description de fichier ajouté dans un second temps",
            }

            # puisque l'envoi du fichier c'est bien passé,
            # on ajoute ce fichier à une data
            rf = nklD.post_datas_files(nklT_testUserunakala2, dataIdentifier, fileInfos)
            print(rf)

            return rf


def delete_datas_files_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer un objet avec un file
    # on ajoute un file a cette data
    # puis on supprimme ce file

    # creation d'une nouvelle data
    rd = post_datas_test()

    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]

        # envoi d'un nouveau fichier pour obtenir son sha1
        pathFile = "./../test/files/test_0001-0002.jpg"
        r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)

        print(r)

        if r.isSuccess:
            print("le sha1 est", r.dictVals["sha1"])

            fileInfos = {
                "sha1": r.dictVals["sha1"],
                "description": "ma belle description de fichier ajouté dans un second temps",
            }

            # puisque l'envoi du fichier c'est bien passé,
            # on ajoute ce fichier à une data
            rf = nklD.post_datas_files(nklT_testUserunakala2, dataIdentifier, fileInfos)
            print(rf)

            if rf.isSuccess:
                # puisque l'ajout du file a fonctionné on test sa suppression
                fileIdentifier = r.dictVals["sha1"]
                rd = nklD.delete_datas_files(
                    nklT_testUserunakala2, dataIdentifier, fileIdentifier
                )

                print(rd)


def get_datas_metadatas_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.8a0czf6y"
    r = nklD.get_datas_metadatas(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.339evx18"
    r = nklD.get_datas_metadatas(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe avec format dc>>")
    dataIdentifier = "10.34847/nkl.339evx18"
    r = nklD.get_datas_metadatas(nklT_prodEmpty, dataIdentifier, metadataFormat="dc")
    print(r)

    print("Test d'un dataIdentifier qui existe avec format qdc>>")
    dataIdentifier = "10.34847/nkl.339evx18"
    r = nklD.get_datas_metadatas(nklT_prodEmpty, dataIdentifier, metadataFormat="qdc")
    print(r)


def post_datas_metadatas_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )

    # on creer une data qu'on va completer ensuite
    rd = post_datas_test()
    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]
        # ajout d'un titre en anglais
        dictValsNewMetas = {
            "value": "coooooool",
            "lang": "en",
            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
            "propertyUri": "http://purl.org/dc/terms/subject",
        }
        nklD = nklAPI_Datas()
        rm = nklD.post_datas_metadatas(
            nklT_testUserunakala2, dataIdentifier, dictValsNewMetas
        )

        print(rm)


def delete_datas_metadatas_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer une data pour aller supprimer ses subject en anglais
    rc = post_datas_test()

    if rc.isSuccess:
        dataIdentifier = rc.dictVals["payload"]["id"]
        # ajout d'un titre en anglais
        dictValsMetaFilterToDelete = {
            "lang": "en",
            "propertyUri": "http://purl.org/dc/terms/subject",
        }
        # suppression du titre
        rd = nklD.delete_datas_metadatas(
            nklT_testUserunakala2, dataIdentifier, dictValsMetaFilterToDelete
        )
        print(rd)


def get_datas_rights_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.8a0czf6y"
    r = nklD.get_datas_rights(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.339evx18"
    r = nklD.get_datas_rights(nklT_prodEmpty, dataIdentifier)
    print(r)


def post_datas_rights_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer une data qu'on va completer ensuite
    rd = post_datas_test()
    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]
        # ajout d'un rights ROLE_READER à unakala1 de0f2a9b-a198-48a4-8074-db5120187a16
        listDicRights = [
            {"id": "de0f2a9b-a198-48a4-8074-db5120187a16", "role": "ROLE_READER"}
        ]

        rm = nklD.post_datas_rights(
            nklT_testUserunakala2, dataIdentifier, listDicRights
        )

        print(rm)


def delete_datas_rights_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer une data pour aller supprimer les droits du unakala3
    rc = post_datas_test()

    # ajout d'un droit editeur à tnakala ROLE_EDITOR
    if rc.isSuccess:
        dataIdentifier = rc.dictVals["payload"]["id"]
        # ajout d'un rights ROLE_EDITOR à tnakala 26cef362-5bef-11eb-99d1-5254000a365d
        listDicRights = [
            {"id": "26cef362-5bef-11eb-99d1-5254000a365d", "role": "ROLE_EDITOR"}
        ]
        rm = nklD.post_datas_rights(
            nklT_testUserunakala2, dataIdentifier, listDicRights
        )
        print(rm)
        if rm.isSuccess:
            # suppressiondes droits editor
            dictValsRightsFilterToDelete = {"role": "ROLE_EDITOR"}
            # suppression des droits du unakala3
            rd = nklD.delete_datas_rights(
                nklT_testUserunakala2, dataIdentifier, dictValsRightsFilterToDelete
            )
            print(rd)


def get_datas_collections_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.8a0czf6y"
    nklD = nklAPI_Datas()
    r = nklD.get_datas_collections(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.339evx18"
    r = nklD.get_datas_collections(nklT_prodEmpty, dataIdentifier)
    print(r)

    if r.isSuccess:
        for collection in r.dictVals:
            print("Infos collection >> ")
            print(collection["status"])
            for meta in collection["metas"]:
                print(meta["value"])


def post_datas_collections_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer une data qu'on va completer ensuite
    rd = post_datas_test()
    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]
        # ajout de la data à la collection 10.34847/nkl.b9eblul0
        listNewCollection = ["10.34847/nkl.b9eblul0"]

        rm = nklD.post_datas_collections(
            nklT_testUserunakala2, dataIdentifier, listNewCollection
        )

        print(rm)


def delete_datas_collections_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer une data avec collection qu'on va supprimer

    # on creer une data
    rd = post_datas_test()
    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]
        # ajout de la data à la collection 10.34847/nkl.b9eblul0
        listNewCollection = ["10.34847/nkl.b9eblul0"]
        rm = nklD.post_datas_collections(
            nklT_testUserunakala2, dataIdentifier, listNewCollection
        )
        print(rm)

        if rm.isSuccess:
            # la collection a bien été ajouté à la data
            # on va pouvoir la supprimer
            rdel = nklD.delete_datas_collections(
                nklT_testUserunakala2, dataIdentifier, listNewCollection
            )
            print(rdel)


def get_datas_status_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.8a0czf6y"
    r = nklD.get_datas_status(nklT_prodEmpty, dataIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.339evx18"
    r = nklD.get_datas_status(nklT_prodEmpty, dataIdentifier)
    print(r)


def put_datas_status_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    # on creer une data avec un status pending que l'onva passer en published

    # on creer une data
    rd = post_datas_test()
    if rd.isSuccess:
        dataIdentifier = rd.dictVals["payload"]["id"]
        rp = nklD.put_datas_status(nklT_testUserunakala2, dataIdentifier)
        print(rp)


def get_datas_uploads_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    r = nklD.get_datas_uploads(nklT_testUserunakala2)

    print(r)


def post_datas_uploads_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )
    pathFile = "./../test/files/test_0001-0002.jpg"
    r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)

    print(r)

    if r.isSuccess:
        print("le sha1 est", r.dictVals["sha1"])


def delete_datas_uploads_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_testUserunakala2 = nklT.NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )
    pathFile = "./../test/files/test_0001-0002.jpg"
    r = nklD.post_datas_uploads(nklT_testUserunakala2, pathFile)

    print(r)

    if r.isSuccess:
        print("le sha1 est", r.dictVals["sha1"])

        # maintenant que nous avons un sha1, nous pouvons
        # essayer de supprimer ce fichier de l'espace temporaire
        fileIdentifier = r.dictVals["sha1"]

        rd = nklD.delete_datas_uploads(nklT_testUserunakala2, fileIdentifier)
        print(rd)


def get_iiif_infoJson_test():
    # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = nklT.NklTarget(isNakalaProd=True, apiKey="")

    print("Test d'un dataIdentifier qui n'existe pas >> ")
    dataIdentifier = "10.34847/nkl.8a0czf6y"
    fileIdentifier = "abcde"
    r = nklD.get_iiif_infoJson(nklT_prodEmpty, dataIdentifier, fileIdentifier)
    print(r)

    print("Test d'un dataIdentifier qui existe >>")
    dataIdentifier = "10.34847/nkl.339evx18"
    fileIdentifier = "f50f7a77785ee1d36e322febb652fcad54e1004a"
    r = nklD.get_iiif_infoJson(nklT_prodEmpty, dataIdentifier, fileIdentifier)
    print(r)


# get_datas_test()
#put_datas_test()
# delete_datas_test()
# post_datas_test()


# get_datas_files_test()
# post_datas_files_test()
# delete_datas_files_test()

# get_datas_metadatas_test()
# post_datas_metadatas_test()
# delete_datas_metadatas_test()

# get_datas_rights_test()
# post_datas_rights_test()
# delete_datas_rights_test()

get_datas_collections_test()
# post_datas_collections_test()
# delete_datas_collections_test()

# get_datas_status_test()
# put_datas_status_test()

# post_datas_uploads_test()
# get_datas_uploads_test()
# delete_datas_uploads_test()


# get_iiif_infoJson_test()
