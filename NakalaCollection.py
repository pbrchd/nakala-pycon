# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:28:54 2022

Created : @author: pbrochard

"""

from nklAPI_Collections import nklAPI_Collections
from NklTarget import NklTarget
import logging


class NakalaCollection(object):
    """
    Turns a dictionary into a class
    """

    # ----------------------------------------------------------------------
    def __init__(self, identifier=None, lang="fr", isNakalaProd=True):
        """Constructor"""
        self.lang = lang
        self.listeMeta = []
        self.listeCreator = []
        self.listeCreatorURI = []
        self.listeMotsClefs = []
        self.listeSpatial = []
        self.listeSpatialURI = []
        self.listeSource = []
        self.listeSourceURI = []
        self.listeReferences = []
        self.listePublisher = []
        self.listePublisherURI = []
        self.listeProvenance = []
        self.listeProvenanceURI = []
        self.listeModified = []
        self.listRelation = []
        self.listRelationURI = []
        if identifier is not None:
            self.identifier = identifier
            self.fromIdentifier(identifier, isNakalaProd)

    def fromIdentifier(self, identifier, isNakalaProd=True):
        # un objet de connexion à nakala production (sans key api)
        listeCreators = []
        listeCreatorsURI = []
        listeMotsClefs = []
        listeSpatial = []
        listeSpatialURI = []
        listeSource = []
        listeSourceURI = []
        listeReferences = []
        listePublisher = []
        listePublisherURI = []
        listeProvenance = []
        listeProvenanceURI = []
        listeModified = []
        listeRelation = []
        listeRelationURI = []
        nklT_prod = NklTarget(isNakalaProd, apiKey="")
        nklC = nklAPI_Collections()
        r = nklC.get_collections(nklT_prod, identifier, metadataFormat="qdc")

        for element in r.dictVals["metas"]:
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/title"
                and element["lang"] == self.lang
            ):
                setattr(self, "title", element["value"])
            if (
                element["propertyUri"] == "http://nakala.fr/terms#created"
                and element["lang"] == lang
            ):
                setattr(self, "created", element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/description"
                and element["lang"] == self.lang
            ):
                setattr(self, "description", element["value"])
            if element["propertyUri"] == "http://purl.org/dc/terms/creator":
                listeCreators.append(element["value"])
            if element["propertyUri"] == "http://purl.org/dc/terms/date":
                setattr(self, "date", element["value"])
            if element["propertyUri"] == "http://purl.org/dc/terms/hasVersion":
                setattr(self, "hasVersion", element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/subject"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
                and element["lang"] == self.lang
            ):
                listeMotsClefs.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/spatial"
                and element["lang"] == self.lang
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeSpatial.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/spatial"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeSpatialURI.append(element["value"])
            if ():
                listeSourceURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/isReferencedBy"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeReferences.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/publisher"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listePublisher.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/publisher"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listePublisherURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/modified"
                and element["typeUri"] == "http://purl.org/dc/terms/W3CDTF"
            ):
                listeModified.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/provenance"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeProvenance.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/provenance"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeProvenanceURI.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/relation"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#string"
            ):
                listeRelation.append(element["value"])
            if (
                element["propertyUri"] == "http://purl.org/dc/terms/relation"
                and element["typeUri"] == "http://www.w3.org/2001/XMLSchema#anyURI"
            ):
                listeRelationURI.append(element["value"])

        setattr(self, "creator", listeCreators)
        setattr(self, "creatorURI", listeCreatorsURI)
        setattr(self, "subject", listeMotsClefs)
        setattr(self, "spatial", listeSpatial)
        setattr(self, "spatialURI", listeSpatialURI)
        setattr(self, "source", listeSource)
        setattr(self, "sourceURI", listeSource)
        setattr(self, "references", listeReferences)
        setattr(self, "publisher", listePublisher)
        setattr(self, "publisherURI", listePublisherURI)
        setattr(self, "provenance", listeProvenance)
        setattr(self, "provenanceURI", listeProvenanceURI)
        setattr(self, "modified", listeModified)
        setattr(self, "relation", listeRelation)
        setattr(self, "relationURI", listeRelationURI)
        logging.info("Instantiation de lobject NakalaCollection")

    def setMeta(self, param, value):
        """
        ajouter la metadonnee
        """
        setattr(self, param, value)

    def getMeta(self, param):
        """
        ajouter la metadonnee
        """
        return getattr(self, param)

    def getDictMetaURI(self):
        """
        renvoie le dict["metas"]
        """
        vocab = "http://purl.org/dc/terms/"
        listeVocab = [
            "subjectURI",
            "descriptionURI",
            "publisherURI",
            "creatorURI",
            "contributorURI",
            "dateURI",
            "typeURI",
            "formatURI",
            "identifierURI",
            "sourceURI",
            "languageURI",
            "relationURI",
            "coverageURI",
            "rightsURI",
            "audienceURI",
            "alternativeURI",
            "tableOfContentsURI",
            "abstractURI",
            "createdURI",
            "validURI",
            "availableURI",
            "issuedURI",
            "modifiedURI",
            "extentURI",
            "mediumURI",
            "isVersionOfURI",
            "hasVersionURI",
            "isReplacedByURI",
            "replacesURI",
            "isRequiredByURI",
            "requiresURI",
            "isPartOfURI",
            "hasPartURI",
            "isReferencedByURI",
            "referencesURI",
            "isFormatOfURI",
            "hasFormatURI",
            "conformsToURI",
            "spatialURI",
            "temporalURI",
            "mediatorURI",
            "dateAcceptedURI",
            "dateCopyrightedURI",
            "dateSubmittedURI",
            "educationLevelURI",
            "accessRightsURI",
            "bibliographicCitationURI",
            "licenseURI",
            "rightsHolderURI",
            "provenanceURI",
            "instructionalMethodURI",
            "accrualMethodURI",
            "accrualPeriodicityURI",
            "accrualPolicyURI",
        ]
        for param, element in self.__dict__.items():
            if type(getattr(self, param)) == str and param in listeVocab:
                self.listeMeta.append(
                    {
                        "value": element,
                        "lang": self.lang,
                        "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
                        "propertyUri": vocab + param[:-3],
                    }
                )
            if type(getattr(self, param)) == list and param in listeVocab:
                for elem in element:
                    self.listeMeta.append(
                        {
                            "value": elem,
                            "lang": self.lang,
                            "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
                            "propertyUri": vocab + param[:-3],
                        }
                    )

    def getDictMetaString(self):
        """
        renvoie le dict["metas"]
        """
        listeNakala = ["title", "created"]
        listeVocab = [
            "subject",
            "description",
            "publisher",
            "creator",
            "contributor",
            "date",
            "type",
            "format",
            "identifier",
            "source",
            "language",
            "relation",
            "coverage",
            "rights",
            "audience",
            "alternative",
            "tableOfContents",
            "abstract",
            "valid",
            "available",
            "issued",
            "modified",
            "extent",
            "medium",
            "isVersionOf",
            "hasVersion",
            "isReplacedBy",
            "replaces",
            "isRequiredBy",
            "requires",
            "isPartOf",
            "hasPart",
            "isReferencedBy",
            "references",
            "isFormatOf",
            "hasFormat",
            "conformsTo",
            "spatial",
            "temporal",
            "mediator",
            "dateAccepted",
            "dateCopyrighted",
            "dateSubmitted",
            "educationLevel",
            "accessRights",
            "bibliographicCitation",
            "rightsHolder",
            "provenance",
            "instructionalMethod",
            "accrualMethod",
            "accrualPeriodicity",
            "accrualPolicy",
        ]
        for param, element in self.__dict__.items():
            if param in listeNakala:
                vocab = "http://nakala.fr/terms#"
            if param in listeVocab:
                vocab = "http://purl.org/dc/terms/"
            if type(getattr(self, param)) == str and param in [*listeVocab, "title"]:
                self.listeMeta.append(
                    {
                        "value": element,
                        "lang": self.lang,
                        "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                        "propertyUri": vocab + param,
                    }
                )
            if type(getattr(self, param)) == list and param in [*listeVocab, "title"]:
                for elem in element:
                    self.listeMeta.append(
                        {
                            "value": elem,
                            "lang": self.lang,
                            "typeUri": "http://www.w3.org/2001/XMLSchema#string",
                            "propertyUri": vocab + param,
                        }
                    )

    def setMeta(self, param, value):
        """
        ajouter la metadonnee
        """
        setattr(self, param, value)

    def getMeta(self, param):
        """
        ajouter la metadonnee
        """
        return getattr(self, param)

    def getMetaNakala(self):
        """
        renvoie les metadonnees au format Nakala
        """
        self.getDictMetaURI()
        self.getDictMetaString()
        return self.listeMeta
