# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

Created : @author: Michael Nauge (Université de Poitiers)
Forked : @pbrochard

"""


from NklTarget import NklTarget
from nklAPI_Vocabularies import nklAPI_Vocabularies


def get_vocabularies_licenses_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    nklV = nklAPI_Vocabularies()
    r = nklV.get_vocabularies_licenses(nklT_test)
    print(r)

    print("----------------")
    if r.isSuccess:
        for licence in r.dictVals:
            print("code:", licence["code"])
            print("name:", licence["name"])
            print("url:", licence["url"])


def get_vocabularies_datatypes():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )
    nklV = nklAPI_Vocabularies()

    r = nklV.get_vocabularies_datatypes(nklT_test)
    print(r)


def get_vocabularies_properties_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    nklV = nklAPI_Vocabularies()
    r = nklV.get_vocabularies_properties(nklT_test)
    print(r)


def get_vocabularies_metadatatypes_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    nklV = nklAPI_Vocabularies()
    r = nklV.get_vocabularies_metadatatypes(nklT_test)
    print(r)


def get_vocabularies_languages_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef"
    )

    """
    print("recherche par defaut")
    r = nklV.get_vocabularies_languages(nklT_test)
    print(r)   
    
    print("recherche avec valeurs")
    r = nklV.get_vocabularies_languages(nklT_test,code="fr")
    print(r)    
    """
    print("recherche avec valeurs")
    nklV = nklAPI_Vocabularies()
    r = nklV.get_vocabularies_languages(nklT_test, q="french")
    print(r)

    if r.isSuccess:
        print("------")
        for lang in r.dictVals:
            print(lang)


get_vocabularies_licenses_test()
get_vocabularies_datatypes()
get_vocabularies_properties_test()
get_vocabularies_metadatatypes_test()
get_vocabularies_languages_test()
