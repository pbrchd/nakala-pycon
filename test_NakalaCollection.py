# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:28:54 2022

Created : @author: pbrochard

"""

from NakalaCollection import NakalaCollection

if __name__ == "__main__":
    Element = NakalaCollection(identifier="10.34847/nkl.b5afv4oj", lang="fr")
    print(Element.__dict__)
    print(Element.description)
    print(", ".join(Element.creator))
