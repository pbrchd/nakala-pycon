# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 11:56:22 2021

Created :@author: Michael Nauge, Université de Poitiers

"""

from NklTarget import *
from NklResponse import *

from nklAPI_Collections import *
from nklAPI_Datas import *
from nklAPI_Groups import *
from nklAPI_Users import *
from nklAPI_Vocabularies import *

from nklUtils import *
from nklPullCorpus import *
