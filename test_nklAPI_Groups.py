# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

Created : @author: Michael Nauge (Université de Poitiers)

"""


from NklTarget import NklTarget
from nklAPI_Groups import nklAPI_Groups


def get_groups_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    test = NklTarget(isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef")

    print("Test d'un group Identifier qui n'existe pas >> ")
    groupIdentifier = "db5f5980-056e-11ec-9b31-52540084ccd0"
    nklG = nklAPI_Groups()
    r = nklG.get_groups(test, groupIdentifier)
    print(r)

    print("Test d'un group Identifier qui existe >>")
    groupIdentifier = "cb5f5980-056e-11ec-9b31-52540084ccd3"
    r = nklG.get_groups(test, groupIdentifier)
    if r.isSuccess:
        print("group finded")
        print("id : ", r.dictVals["id"])
        print("name:", r.dictVals["name"])
        print("type :", r.dictVals["type"])

        print("users")
        for user in r.dictVals["users"]:
            print(user)
    else:
        print(r)


def put_groups_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    test = NklTarget(isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef")

    groupIdentifier = "cb5f5980-056e-11ec-9b31-52540084ccd3"

    dicoGroups = {}
    dicoGroups["id"] = "cb5f5980-056e-11ec-9b31-52540084ccd3"

    dicoGroups["name"] = "etablissement42"

    dicoGroups["users"] = []

    dicoGroups["users"].append("unakala1")
    dicoGroups["users"].append("unakala2")

    nklG = nklAPI_Groups()
    r = nklG.put_groups(test, groupIdentifier, dicoGroups)
    print(r)


def post_groups_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    test = NklTarget(isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef")

    dicoGroups = {}

    dicoGroups["name"] = "groupCreatedByAPI"

    dicoGroups["users"] = []

    dicoGroups["users"].append("unakala1")
    dicoGroups["users"].append("unakala2")
    dicoGroups["users"].append("unakala3")

    nklG = nklAPI_Groups()
    r = nklG.post_groups(test, dicoGroups)

    if r.isSuccess:
        print(r)
        idGroup = r.dictVals["payload"]["id"]
        print("group identifier", idGroup)

        return idGroup

    else:
        print(r)

        return ""


def delete_groups_test():
    # pour supprimer un group on va commencer par en creer un
    idGroup = post_groups_test()

    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    test = NklTarget(isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef")

    nklG = nklAPI_Groups()
    r = nklG.delete_groups(test, idGroup)

    print(r)


def search_groups_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    test = NklTarget(isNakalaProd=False, apiKey="000000000-00ab-cdef-0000-000000abcdef")

    q = "Huma-Num-test-Admin"
    nklG = nklAPI_Groups()
    r = nklG.search_groups(test, q, order="asc", page=1, limit=10)

    if r.isSuccess:
        print(r)

        print("nb responses finded", len(r.dictVals))
        for vals in r.dictVals:
            print("-------------------")
            print("id : ", vals["id"])
            print("name:", vals["name"])
            print("type :", vals["type"])

            print("users")
            for user in vals["users"]:
                print(user)
    else:
        print(r)


post_groups_test()
# delete_groups_test()
put_groups_test()
get_groups_test()
search_groups_test()
