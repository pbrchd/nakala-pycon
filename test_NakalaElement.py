# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:28:54 2022

Created : @author: pbrochard

"""

from NklTarget import NklTarget
from nklAPI_Collections import nklAPI_Collections
from NakalaElement import NakalaElement
from nklAPI_Datas import nklAPI_Datas


if __name__ == "__main__":
        # un objet de connexion à nakala production (sans key api)
    nklT_prodEmpty = NklTarget(isNakalaProd=True, apiKey="")
    nklC = nklAPI_Collections()
    dataIdentifier = "10.34847/nkl.28632y7o"
    r = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier)
    if r.isSuccess:
        lastPage = r.dictVals["lastPage"]
    for i in range(1, lastPage + 1):
        r_last = nklC.get_collections_datas(nklT_prodEmpty, dataIdentifier, page=i)
        for element in r_last.dictVals["data"]:
            Element = NakalaElement("fr")
            Element.fromNakalaDict(element)
            print(Element.title)
