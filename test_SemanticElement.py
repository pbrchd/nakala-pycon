# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 09:28:54 2022

Created : @author: pbrochard

"""

from SemanticElement import SemanticElement


if __name__ == "__main__":
    # requete = "http://www.nakala.fr/10.34847/nkl.c4aamk8s"
    Test = SemanticElement("fr")
    # Test.interrogation(requete)
    #requete = "http://www.wikidata.org/entity/Q546628"
    requete = "http://www.wikidata.org/entity/Q113810746"

    #Test.interrogationId(requete)
    print(Test.interrogation(requete))
    #value = Test.interrogationId(requete)
#    Test.debug()
