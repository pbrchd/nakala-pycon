# -*- coding: utf-8 -*-
"""
Created on Mon Aug 23 16:39:24 2021

Created : @author: Michael Nauge (Université de Poitiers)

"""


from NklTarget import NklTarget
from nklAPI_Users import nklAPI_Users


def get_users_me_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )

    nklU = nklAPI_Users()
    r = nklU.get_users_me(nklT_test)
    print(r)

    print("----------------")
    if r.isSuccess:
        print("username:", r.dictVals["username"])
        print("givenname:", r.dictVals["givenname"])
        print("surname:", r.dictVals["surname"])
        print("mail:", r.dictVals["mail"])
        print("photo:", r.dictVals["photo"])
        print("firstLogin:", r.dictVals["firstLogin"])
        print("lastLogin:", r.dictVals["lastLogin"])
        print("roles:", r.dictVals["roles"])
        print("apiKey:", r.dictVals["apiKey"])
        print("fullname:", r.dictVals["fullname"])


def post_users_datas_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )
    nklU = nklAPI_Users()
    page = 50
    r = nklU.post_users_datas(nklT_test, "deposited", 1, 10)
    if r.isSuccess:
        nbrePages = int(int(r.dictVals["totalRecords"]) / page + 1)
    for i in range(1, nbrePages):
        r = nklU.post_users_datas(nklT_test, "deposited", i, page)
        if r.isSuccess:
            for element in r.dictVals["data"]:
                print(element["collectionsIds"])
                print(element["identifier"])
                print("------------------------------------")


def post_users_collections_test():
    # un objet de connexion à nakala test (avec key api)
    # unakala2 : 000000000-00ab-cdef-0000-000000abcdef
    nklT_test = NklTarget(
        isNakalaProd=False, apiKey="01234567-89ab-cdef-0123-456789abcdef"
    )
    nklU = nklAPI_Users()
    page = 50
    r = nklU.post_users_collections(nklT_test, "deposited", 1, 10)
    if r.isSuccess:
        nbrePages = int(int(r.dictVals["totalRecords"]) / page + 1)

    for i in range(1, nbrePages + 1):
        r = nklU.post_users_collections(nklT_test, "deposited", i, page)
        if r.isSuccess:
            for element in r.dictVals["data"]:
                print(element["uri"])
                for metas in element["metas"]:
                    if (
                        metas["propertyUri"] == "http://nakala.fr/terms#title"
                        and metas["lang"] == "fr"
                        and metas["typeUri"]
                        == "http://www.w3.org/2001/XMLSchema#string"
                    ):
                        print(metas["value"])
                print("------------------------------------")


# get_users_me_test()

# post_users_datas_test()

post_users_collections_test()
